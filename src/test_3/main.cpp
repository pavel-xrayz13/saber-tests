#define _CRT_SECURE_NO_WARNINGS

#include <cstdint>
#include <iostream>
#include <string>
#include <random>
#include <cassert>
#include <unordered_map>

struct ListNode {
	ListNode * prev{ nullptr };
	ListNode * next{ nullptr };
	ListNode * rand{ nullptr }; // ��������� �� ������������ ������� ������� ������ ���� NULL
	std::string data;

	ListNode() = default;
	~ListNode() = default;
};

class List {
public:
	typedef ListNode* iterator;

	List() : head(nullptr), tail(nullptr), count(0) {}
	~List();
	ListNode* insert(const std::string& str, bool add_rand_list_ptr = false); // for tests
	void generate_rand_ptrs();
	void print_all(); // for tests

	iterator begin() { return this->head; }
	iterator end() { return this->tail->next; }

	void Serialize(FILE * file); // ���������� � ���� (���� ������ � ������� fopen(path, "wb"))
	void Deserialize(FILE * file); // �������� �� ����� (���� ������ � ������� fopen(path, "rb"))
private:
	ListNode * generate_random_node_pointer();
	ListNode * get_random_node_or_null();
	ListNode * get_list_entry_by_index(int index);
	ListNode * head;
	ListNode * tail;
	int count;
};

List::~List()
{
	auto reverse_current = this->end();
	auto reverse_end = this->begin()->prev;

	while (reverse_current != reverse_end)
	{
		auto current = reverse_current;
		reverse_current = reverse_current->prev;
		delete current;
	}
}

ListNode* List::insert(const std::string& str, bool add_rand_list_ptr)
{
	auto node = new ListNode();
	if (!head)
	{
		this->head = node;
	}

	if (this->tail)
	{
		node->prev = this->tail;
		this->tail->next = node;
		this->tail = node;
	}
	else
	{
		this->tail = node;
	}

	node->data = std::move(str);

	if (add_rand_list_ptr)
	{
		node->rand = this->get_random_node_or_null();
	}

	++this->count;

	return node;
}

void List::generate_rand_ptrs()
{
	std::mt19937 random_engine;
	std::uniform_int_distribution<int> distribution(0, this->count);
	for (auto current = this->begin(), end = this->end(); current != end; current = current->next)
	{
		auto dist = distribution(random_engine);
		current->rand = this->get_list_entry_by_index(dist);
	}
}

void List::Serialize(FILE * file)
{
	typedef std::unordered_map<ListNode*, int> list_nodes_indexes_t;

	assert(file);
	list_nodes_indexes_t lni;

	fwrite(&this->count, sizeof(this->count), 1, file); // entries count
	
	auto current = this->begin();
	auto end = this->end();

	int index = 0;
	while (current != end)
	{
		auto current_index = index++;
		lni.insert({ current, current_index });

		auto size = current->data.size();
		fwrite(&size, sizeof(std::string::size_type), 1, file);

		auto data = current->data.c_str();
		fwrite(data, size, 1, file);

		current = current->next;
	}

	current = this->begin();
	while (current != end)
	{
		auto current_index = lni[current->rand];
		fwrite(&current_index, sizeof(current_index), 1, file);
		current = current->next;
	}
}

void List::Deserialize(FILE * file)
{
	assert(file);

	typedef std::vector<ListNode*> vector_t;
	typedef char temp_buffer_t[1024];
	vector_t temp_nodes;
	temp_buffer_t temp_buffer;

	int count = 0;
	fread(&count, sizeof(count), 1, file);
	this->count = count;


	for(int i = 0; i < count; ++i)
	{
		std::memset(temp_buffer, 0, sizeof(temp_buffer));

		std::string::size_type size;
		fread(&size, sizeof(size), 1, file);

		assert(size < sizeof(temp_buffer));
		fread(temp_buffer, size, 1, file);

		auto ptr = this->insert(std::string(temp_buffer, size));
		temp_nodes.push_back(ptr);
	}

	auto current = this->begin();
	auto end = this->end();

	while (current != end)
	{
		int index;
		fread(&index, sizeof(index), 1, file);
		current->rand = temp_nodes[index];
		current = current->next;
	}
}

void List::print_all()
{
	auto current = this->begin();
	auto end = this->end();

	while (current != end)
	{
		std::cout << current->data;

		if (current->rand)
		{
			std::cout << " (rand -> " << current->rand->data << " )";
		}

		std::cout << std::endl;

		current = current->next;
	}
}

ListNode * List::generate_random_node_pointer()
{
	std::mt19937 random_engine;
	std::uniform_int_distribution<int> distribution(0, this->count);

	int dist = 0;
	for (auto i = 0; i < 3; ++i)
	{
		dist = distribution(random_engine);
	}

	return this->get_list_entry_by_index(dist);
}

ListNode * List::get_random_node_or_null()
{
	if (!this->count)
	{
		return nullptr;
	}

	return this->generate_random_node_pointer();
}

ListNode * List::get_list_entry_by_index(int index)
{
	int counter = 0;
	auto current = this->begin();
	auto end = this->end();

	while (current != nullptr)
	{
		if (index == counter)
		{
			break;
		}

		current = current->next;
		++counter;
	}

	return current;
}

int main(int argc, char** argv)
{
	const char* test_file = "./test_3.bin";

	{
		auto fd = fopen(test_file, "wb");
		List list_writer;
		list_writer.insert("I'll think about that tomorrow.");
		list_writer.insert("Tomorrow is another day.");
		list_writer.insert("(c) Scarlett O'hara");
		list_writer.insert("You may have a battle more than once to win it.");
		list_writer.insert("(c) Margaret Thatcher");
		list_writer.generate_rand_ptrs();
		list_writer.Serialize(fd);
		fclose(fd);
	}

	{
		auto fd = fopen(test_file, "rb");
		List list_reader;
		list_reader.Deserialize(fd);
		list_reader.print_all();
		fclose(fd);
	}

	return 0;
}