#include <cstdint>
#include <iostream>

void RemoveDups(char* pStr)
{
	auto length = std::strlen(pStr);
	auto temp = reinterpret_cast<char*>(_alloca(length * sizeof(char)));
	std::memset(temp, 0, length);

	auto end = pStr + length;
	auto curchar = ' ';

	for(auto current = pStr, temp_current = temp; current != end; ++current)
	{
		if (*current == curchar)
		{
			continue;
		}

		curchar = *current;
		*(temp_current++) = *current;
	}

	std::memcpy(pStr, temp, length);
}

int main(int argc, char** argv)
{
	char pStr1[] = "AAA BBB AAA";
	char pStr2[] = "AAAAA BBBBB AAAAA BBBBB CCCCCC DDDDD FFFFFFFFF EEEEEEERRRRRRIIIII OOOOOO PPPPPPP";
	RemoveDups(pStr1);
	RemoveDups(pStr2);
	std::cout << pStr1 << std::endl;
	std::cout << pStr2 << std::endl;
	return 0;
}