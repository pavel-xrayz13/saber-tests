#include <cstdint>
#include <iostream>
#include <type_traits>


template<typename type>
struct bits_count_traits;

template<>
struct bits_count_traits<int8_t>
{
	static const size_t count = 8;
};

template<>
struct bits_count_traits<int16_t>
{
	static const size_t count = 16;
};

template<>
struct bits_count_traits<int32_t>
{
	static const size_t count = 32;
};

template<>
struct bits_count_traits<int64_t>
{
	static const size_t count = 64;
};

template<typename value_type>
void printf_signed_as_binary(value_type val)
{
	static_assert(std::is_signed<value_type>::value, "Input type must be signed");
	typedef typename bits_count_traits<value_type> traits_type;

	auto local = val;
	char data[traits_type::count + 1] = { 0 }; // plus NULL-terminator

	for (int iter = traits_type::count - 1; iter >= 0; --iter, local >>= 1)
	{
		data[iter] = (local & 1) ? '1' : '0';
	}

	std::cout << "Input value: " << val << "; It's binary representation is: " << data << std::endl;
}

int main(int argc, char** argv)
{
	printf_signed_as_binary<int8_t>(98);
	printf_signed_as_binary<int8_t>(-53);
	printf_signed_as_binary<int16_t>(1567);
	printf_signed_as_binary<int16_t>(-15434);
	printf_signed_as_binary<int32_t>(1278457);
	printf_signed_as_binary<int32_t>(-278457);
	printf_signed_as_binary<int64_t>(327845799);
	printf_signed_as_binary<int64_t>(-5278557981);

	return 0;
}