for %%i in ("%~dp0..") do set "ROOTDIR=%%~fi"
set BUILD_ROOT=%ROOTDIR%\build
del /s /q %BUILD_ROOT%
mkdir %BUILD_ROOT%
pushd %BUILD_ROOT%
cmake -G "Visual Studio 14 Win64" %ROOTDIR%
popd
pause